package com.pokemon.jv.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pokemon.domain.interactor.DefaultObserver
import com.pokemon.domain.interactor.GetEvolutionChainUseCase
import com.pokemon.domain.interactor.GetPokemonDetailUseCase
import com.pokemon.domain.interactor.GetPokemonEncounterUseCase
import com.pokemon.domain.model.EvolutionChainItem
import com.pokemon.domain.model.LocationAreaEncounterItem
import com.pokemon.domain.model.PokemonItem
import com.pokemon.jv.commons.Event
import com.pokemon.jv.model.DetailModel
import com.pokemon.jv.model.PokemonItemModel
import com.pokemon.jv.model.mapper.PokemonDetailModelMapper
import javax.inject.Inject

class PokemonDetailViewModel @Inject constructor(
    private val getPokemonDetailUseCase: GetPokemonDetailUseCase,
    private val getEvolutionChainUseCase: GetEvolutionChainUseCase,
    private val getPokemonEncounterUseCase: GetPokemonEncounterUseCase
) : ViewModel() {
    private val _loading = MutableLiveData<Event<Boolean>>()
    val loading: LiveData<Event<Boolean>> get() = _loading
    private val _evolutionChange = MutableLiveData<List<DetailModel>>()
    val evolutionChange: LiveData<List<DetailModel>> get() = _evolutionChange
    private val _pokemonEncounter = MutableLiveData<List<DetailModel>>()
    val pokemonEncounter: LiveData<List<DetailModel>> get() = _pokemonEncounter
    private val _itemPokemon = MutableLiveData<PokemonItemModel>()
    val itemPokemon: LiveData<PokemonItemModel> get() = _itemPokemon
    private val _error = MutableLiveData<Event<String>>()
    val error: LiveData<Event<String>> get() = _error

    fun getPokemonDetail(pokemonId: Int) {
        getPokemonDetailUseCase.bindParams(pokemonId)
        getPokemonDetailUseCase.execute(PokemonDetailObserver())
        getPokemonEncounterUseCase.bindParams(pokemonId)
        getPokemonEncounterUseCase.execute(PokemonEncounterObserver())
        getEvolutionChainUseCase.bindParams(pokemonId)
        getEvolutionChainUseCase.execute(EvolutionChainObserver())
    }

    inner class EvolutionChainObserver : DefaultObserver<List<EvolutionChainItem>>() {
        override fun onStart() {
            super.onStart()
            _loading.value = Event.buildEvent(true)
        }

        override fun onNext(t: List<EvolutionChainItem>) {
            super.onNext(t)
            _evolutionChange.value = PokemonDetailModelMapper.transformEvolutionChain(t)
        }

        override fun onError(e: Throwable) {
            super.onError(e)
            _loading.value = Event.buildEvent(false)
            if (e is NullPointerException) {
                _error.value = Event.buildEvent("No pokemon data")
                return
            }
            _error.value = Event.buildEvent(e.message.orEmpty())
        }

        override fun onComplete() {
            super.onComplete()
            _loading.value = Event.buildEvent(false)
        }
    }

    inner class PokemonEncounterObserver : DefaultObserver<List<LocationAreaEncounterItem>>() {
        override fun onStart() {
            super.onStart()
            _loading.value = Event.buildEvent(true)
        }

        override fun onNext(t: List<LocationAreaEncounterItem>) {
            super.onNext(t)
            _pokemonEncounter.value = PokemonDetailModelMapper.transformLocation(t)
        }

        override fun onError(e: Throwable) {
            super.onError(e)
            _loading.value = Event.buildEvent(false)
            if (e is NullPointerException) {
                _error.value = Event.buildEvent("No pokemon data")
                return
            }
            _error.value = Event.buildEvent(e.message.orEmpty())
        }

        override fun onComplete() {
            super.onComplete()
            _loading.value = Event.buildEvent(false)
        }
    }

    inner class PokemonDetailObserver : DefaultObserver<PokemonItem>() {
        override fun onStart() {
            super.onStart()
            _loading.value = Event.buildEvent(true)
        }

        override fun onNext(t: PokemonItem) {
            super.onNext(t)
            _itemPokemon.value = PokemonDetailModelMapper.transform(t)
        }

        override fun onError(e: Throwable) {
            super.onError(e)
            _loading.value = Event.buildEvent(false)
            if (e is NullPointerException) {
                _error.value = Event.buildEvent("No pokemon data")
                return
            }
            _error.value = Event.buildEvent(e.message.orEmpty())
        }

        override fun onComplete() {
            super.onComplete()
            _loading.value = Event.buildEvent(false)
        }
    }

    override fun onCleared() {
        getPokemonDetailUseCase.unSubscribe()
        getEvolutionChainUseCase.unSubscribe()
        getPokemonEncounterUseCase.unSubscribe()
    }
}