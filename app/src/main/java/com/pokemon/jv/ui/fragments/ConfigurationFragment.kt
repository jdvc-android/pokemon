package com.pokemon.jv.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.pokemon.jv.databinding.FragmentConfigurationBinding
import com.pokemon.jv.di.component.DaggerConfigurationFragmentComponent
import com.pokemon.jv.ui.activities.LoginActivity
import com.pokemon.jv.ui.base.BaseFragment
import com.pokemon.jv.ui.viewmodel.ConfigurationViewModel
import javax.inject.Inject

class ConfigurationFragment : BaseFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: ConfigurationViewModel by viewModels(factoryProducer = { viewModelFactory })
    lateinit var binding: FragmentConfigurationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        buildInjection()
    }

    private fun buildInjection() {
        DaggerConfigurationFragmentComponent.builder()
            .applicationComponent(getAndroidApplication().getApplicationComponent())
            .build().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentConfigurationBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initUI()
        setupObserver()
    }

    private fun setupObserver() {
        viewModel.showLogin.observe(viewLifecycleOwner, Observer { goLogin() })
    }

    private fun goLogin() {
        val intent = Intent(activity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        activity?.finish()
    }

    private fun initUI() {
        binding.btnLogout.setOnClickListener {
            viewModel.logout()
        }
    }
}