package com.pokemon.jv.ui.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.ViewModelProvider
import com.pokemon.jv.R
import com.pokemon.jv.commons.observeEvent
import com.pokemon.jv.databinding.ActivityLoginBinding
import com.pokemon.jv.di.component.DaggerLoginActivityComponent
import com.pokemon.jv.ui.base.BaseActivity
import com.pokemon.jv.ui.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : BaseActivity() {
    lateinit var binding: ActivityLoginBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: LoginViewModel by viewModels(factoryProducer = { viewModelFactory })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        buildInjection()
        initUI()
        observeUI()
    }

    private fun buildInjection() {
        DaggerLoginActivityComponent.builder().applicationComponent(appComponent)
            .build()
            .inject(this)
    }

    private fun observeUI() {
        viewModel.showHome.observeEvent(this) {
            if (it) {
                goHome()
            } else {
                showErrorMessage(getString(R.string.error_text))
            }
        }
    }

    private fun goHome() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun getUsername(): String {
        return etUsername.text.toString()
    }

    private fun getPassword(): String {
        return etPassword.text.toString()
    }

    private fun initUI() {
        viewModel.validateScreen()
        binding.btnLogin.setOnClickListener {
            viewModel.validate(getUsername(), getPassword())
        }
    }

    private fun showErrorMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}