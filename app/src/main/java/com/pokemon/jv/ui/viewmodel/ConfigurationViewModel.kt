package com.pokemon.jv.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pokemon.data.sharedPreferences.PreferencesManager
import com.pokemon.jv.commons.Event
import javax.inject.Inject

class ConfigurationViewModel @Inject constructor(
    private val preferencesManager: PreferencesManager
) : ViewModel() {
    private val _showLogin = MutableLiveData<Event<Boolean>>()
    val showLogin: LiveData<Event<Boolean>> get() = _showLogin

    fun logout() {
        preferencesManager.clear()
        _showLogin.value = Event.buildEvent(true)
    }
}