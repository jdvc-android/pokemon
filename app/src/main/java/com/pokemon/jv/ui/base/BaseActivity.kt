package com.pokemon.jv.ui.base

import androidx.appcompat.app.AppCompatActivity
import com.pokemon.jv.AndroidApplication
import com.pokemon.jv.di.component.ApplicationComponent

abstract class BaseActivity : AppCompatActivity() {
    val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        (application as AndroidApplication).getApplicationComponent()
    }

}