package com.pokemon.jv.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pokemon.jv.commons.observeEvent
import com.pokemon.jv.databinding.FragmentListPokemonBinding
import com.pokemon.jv.di.component.DaggerListPokemonFragmentComponent
import com.pokemon.jv.model.PokemonModel
import com.pokemon.jv.ui.activities.PokemonDetailActivity
import com.pokemon.jv.ui.adapters.ListPokemonAdapter
import com.pokemon.jv.ui.base.BaseFragment
import com.pokemon.jv.ui.viewmodel.ListPokemonViewModel
import kotlinx.android.synthetic.main.fragment_list_pokemon.*
import kotlinx.android.synthetic.main.loading.*
import javax.inject.Inject

class ListPokemonFragment : BaseFragment(), ListPokemonAdapter.ListPokemonCallback {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: ListPokemonViewModel by viewModels(factoryProducer = { viewModelFactory })
    private lateinit var binding: FragmentListPokemonBinding

    @Inject
    lateinit var adapter: ListPokemonAdapter

    private var isLoading = false
    private var data = ArrayList<PokemonModel?>()

    companion object {
        const val LIMIT = 20
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        buildInjection()
    }

    private fun buildInjection() {
        DaggerListPokemonFragmentComponent.builder()
            .applicationComponent(getAndroidApplication().getApplicationComponent())
            .build().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentListPokemonBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initUI()
        setupObserver()
    }

    private fun setupObserver() {
        viewModel.loading.observeEvent(viewLifecycleOwner) { if (it) showLoading() else hideLoading() }
        viewModel.error.observeEvent(viewLifecycleOwner) { showErrorMessage(it) }
        viewModel.listPokemonModel.observe(viewLifecycleOwner, Observer { successListPokemon(it) })
    }

    private fun initUI() {
        adapter.setListPokemonCallback(this)
        binding.rvPokemons.layoutManager = LinearLayoutManager(activity)
        binding.rvPokemons.adapter = adapter
        initScrollListener()
        viewModel.getPokemonList(0, LIMIT)
    }

    private fun initScrollListener() {
        rvPokemons.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(
                @NonNull recyclerView: RecyclerView,
                newState: Int
            ) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(
                @NonNull recyclerView: RecyclerView,
                dx: Int,
                dy: Int
            ) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager =
                    recyclerView.layoutManager as LinearLayoutManager?
                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == adapter.itemCount - 1) {
                        loadMore()
                        isLoading = true
                    }
                }
            }
        })
    }

    private fun successListPokemon(list: List<PokemonModel>?) {
        if (list != null) {
            data.addAll(list)
            adapter.addItems(data)
            isLoading = false
        }
    }

    private fun showLoading() {
        loading.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        loading.visibility = View.GONE
    }

    private fun loadMore() {
        viewModel.getPokemonList(data.size + 1, data.size + LIMIT)
    }

    private fun showErrorMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun onClick(model: PokemonModel, position: Int) {
        val intent = Intent(activity, PokemonDetailActivity::class.java)
        intent.putExtra(PokemonDetailActivity.ITEM, model)
        startActivity(intent)
    }
}