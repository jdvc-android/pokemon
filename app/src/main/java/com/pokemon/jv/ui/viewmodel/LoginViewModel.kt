package com.pokemon.jv.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pokemon.data.sharedPreferences.PreferencesManager
import com.pokemon.jv.commons.Event
import javax.inject.Inject

class LoginViewModel @Inject constructor(private val preferences: PreferencesManager) :
    ViewModel() {
    private val pokemon = "pokemon"
    private val _showHome = MutableLiveData<Event<Boolean>>()
    val showHome: LiveData<Event<Boolean>> get() = _showHome

    fun validate(username: String, password: String) {
        if (username == pokemon && password == pokemon) {
            preferences.save(username)
            _showHome.value = Event.buildEvent(true)
        } else {
            _showHome.value = Event.buildEvent(false)
        }
    }

    fun validateScreen() {
        if (preferences.getUsername != null && preferences.getUsername!!.isNotEmpty()) {
            _showHome.value = Event.buildEvent(true)
        }
    }
}