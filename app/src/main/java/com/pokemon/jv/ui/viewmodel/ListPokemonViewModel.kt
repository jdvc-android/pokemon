package com.pokemon.jv.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pokemon.data.utils.Utilities
import com.pokemon.domain.interactor.DefaultObserver
import com.pokemon.domain.interactor.GetListPokemonUseCase
import com.pokemon.domain.interactor.GetLocalListPokemonUseCase
import com.pokemon.domain.interactor.local.SaveListPokemonUseCase
import com.pokemon.domain.model.PokemonListItem
import com.pokemon.jv.commons.Event
import com.pokemon.jv.model.PokemonModel
import com.pokemon.jv.model.mapper.PokemonModelMapper
import javax.inject.Inject

class ListPokemonViewModel @Inject constructor(
    private val getListPokemonUseCase: GetListPokemonUseCase,
    private val getLocalListPokemonUseCase: GetLocalListPokemonUseCase,
    private val saveListPokemonUseCase: SaveListPokemonUseCase
) : ViewModel() {
    private val _loading = MutableLiveData<Event<Boolean>>()
    val loading: LiveData<Event<Boolean>> get() = _loading
    private val _error = MutableLiveData<Event<String>>()
    val error: LiveData<Event<String>> get() = _error

    private val _listPokemonModel = MutableLiveData<List<PokemonModel>>()
    val listPokemonModel: LiveData<List<PokemonModel>> get() = _listPokemonModel


    override fun onCleared() {
        super.onCleared()
        getListPokemonUseCase.unSubscribe()
        getLocalListPokemonUseCase.unSubscribe()
        saveListPokemonUseCase.unSubscribe()
    }

    fun getPokemonList(offset: Int, limit: Int) {
        Utilities.hasInternetConnection().subscribe { t1: Boolean, t2: Throwable? ->
            if (t1) {
                getListPokemonUseCase.bindParams(offset, limit)
                getListPokemonUseCase.execute(ListPokemonObservable())
            } else {
                getLocalListPokemonUseCase.bindParams(offset, limit)
                getLocalListPokemonUseCase.execute(LocalListPokemonObservable())
            }
        }
    }

    inner class LocalListPokemonObservable : DefaultObserver<List<PokemonListItem>>() {
        override fun onStart() {
            super.onStart()
            _loading.value = Event.buildEvent(true)
        }

        override fun onNext(t: List<PokemonListItem>) {
            super.onNext(t)
            _listPokemonModel.value = PokemonModelMapper.transform(t)
        }

        override fun onError(e: Throwable) {
            super.onError(e)
            _loading.value = Event.buildEvent(false)
            if (e is NullPointerException) {
                _error.value = Event.buildEvent("No pokemon data")
                return
            }
            _error.value = Event.buildEvent(e.message.orEmpty())
        }

        override fun onComplete() {
            super.onComplete()
            _loading.value = Event.buildEvent(false)
        }
    }

    inner class ListPokemonObservable : DefaultObserver<List<PokemonListItem>>() {
        override fun onStart() {
            super.onStart()
            _loading.value = Event.buildEvent(true)
        }

        override fun onNext(t: List<PokemonListItem>) {
            super.onNext(t)
            savePokemon(t)
            _listPokemonModel.value = PokemonModelMapper.transform(t)
        }

        override fun onError(e: Throwable) {
            super.onError(e)
            _loading.value = Event.buildEvent(false)
            if (e is NullPointerException) {
                _error.value = Event.buildEvent("No pokemon data")
                return
            }
            _error.value = Event.buildEvent(e.message.orEmpty())
        }

        override fun onComplete() {
            super.onComplete()
            _loading.value = Event.buildEvent(false)
        }
    }

    private fun savePokemon(data: List<PokemonListItem>) {
        saveListPokemonUseCase.bindParams(data)
        saveListPokemonUseCase.execute(SaveListPokemonObservable())
    }

    inner class SaveListPokemonObservable : DefaultObserver<Void>() {
        override fun onStart() {
            super.onStart()
        }

        override fun onComplete() {
            super.onComplete()
        }
    }
}