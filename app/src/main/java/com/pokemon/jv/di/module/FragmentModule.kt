package com.pokemon.jv.di.module

import androidx.fragment.app.Fragment
import com.pokemon.jv.di.PerFragment
import dagger.Module
import dagger.Provides

@Module
class FragmentModule(private val mFragment: Fragment) {
    @Provides
    @PerFragment
    fun fragment(): Fragment {
        return mFragment
    }
}