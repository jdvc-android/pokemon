package com.pokemon.jv.di.component

import com.pokemon.jv.di.PerFragment
import com.pokemon.jv.di.component.viewmodel.ViewModelModuleFragment
import com.pokemon.jv.di.module.FragmentModule
import com.pokemon.jv.ui.fragments.ListPokemonFragment
import dagger.Component

@PerFragment
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [FragmentModule::class, ViewModelModuleFragment::class]
)
interface ListPokemonFragmentComponent {
    fun inject(fragment: ListPokemonFragment)
}