package com.pokemon.jv.di.component

import com.pokemon.jv.di.PerFragment
import com.pokemon.jv.di.component.viewmodel.ViewModelModuleFragment
import com.pokemon.jv.di.module.FragmentModule
import com.pokemon.jv.ui.fragments.ConfigurationFragment
import dagger.Component

@PerFragment
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [FragmentModule::class, ViewModelModuleFragment::class]
)
interface ConfigurationFragmentComponent {
    fun inject(fragment: ConfigurationFragment)
}