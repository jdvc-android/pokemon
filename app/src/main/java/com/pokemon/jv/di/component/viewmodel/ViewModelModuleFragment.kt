package com.pokemon.jv.di.component.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pokemon.jv.di.PerFragment
import com.pokemon.jv.ui.viewmodel.ConfigurationViewModel
import com.pokemon.jv.ui.viewmodel.ListPokemonViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModuleFragment {
    @Binds
    @PerFragment
    internal abstract fun bindViewModelFactoryFragment(factoryFragment: ViewModelFactoryFrament): ViewModelProvider.Factory

    @Binds
    @PerFragment
    @IntoMap
    @ViewModelKey(ListPokemonViewModel::class)
    abstract fun bindsListPokemonViewModel(listPokemonViewModel: ListPokemonViewModel): ViewModel

    @Binds
    @PerFragment
    @IntoMap
    @ViewModelKey(ConfigurationViewModel::class)
    abstract fun bindsConfigurationViewModel(configurationViewModel: ConfigurationViewModel): ViewModel
}