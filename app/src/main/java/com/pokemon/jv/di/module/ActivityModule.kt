package com.pokemon.jv.di.module

import android.app.Activity
import com.pokemon.jv.di.PerActivity
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val mActivity: Activity) {
    @Provides
    @PerActivity
    fun activity(): Activity {
        return mActivity
    }
}