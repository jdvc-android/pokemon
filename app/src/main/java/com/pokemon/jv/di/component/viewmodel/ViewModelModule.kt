package com.pokemon.jv.di.component.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pokemon.jv.di.PerActivity
import com.pokemon.jv.ui.viewmodel.LoginViewModel
import com.pokemon.jv.ui.viewmodel.PokemonDetailViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @PerActivity
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @PerActivity
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindsLoginViewModel(loginViewModel: LoginViewModel): ViewModel

    @Binds
    @PerActivity
    @IntoMap
    @ViewModelKey(PokemonDetailViewModel::class)
    abstract fun bindsPokemonDetailViewModel(pokemonDetailViewModel: PokemonDetailViewModel): ViewModel
}