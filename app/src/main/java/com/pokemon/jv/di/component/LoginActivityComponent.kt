package com.pokemon.jv.di.component

import com.pokemon.jv.di.PerActivity
import com.pokemon.jv.di.component.viewmodel.ViewModelModule
import com.pokemon.jv.di.module.ActivityModule
import com.pokemon.jv.ui.activities.LoginActivity
import dagger.Component

@PerActivity
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [ActivityModule::class, ViewModelModule::class]
)
interface LoginActivityComponent {
    fun inject(activity: LoginActivity)
}