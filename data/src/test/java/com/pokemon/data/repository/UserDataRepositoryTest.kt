package com.pokemon.data.repository

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.pokemon.data.FakeValues.getFakeEvolutionChainItem
import com.pokemon.data.FakeValues.getFakeNamedApiResourceList
import com.pokemon.data.FakeValues.getFakePokemon
import com.pokemon.data.FakeValues.getFakePokemonEncounterList
import com.pokemon.data.network.RestApi
import io.reactivex.Observable
import org.junit.Test
import org.mockito.Mockito.verify

class UserDataRepositoryTest {

    @Test
    fun `given params when getPokemonList then getPokemonList is called`() {
        /* Given */
        val params1 = 1
        val params2 = 2
        val mock = mock<RestApi> {
            on { getPokemonList(params1, params2) } doReturn Observable.just(
                getFakeNamedApiResourceList()
            )
        }
        val sutRepository = UserDataRepository(mock)

        /* When */
        sutRepository.getPokemonList(params1, params2)

        /* Then */
        verify(mock).getPokemonList(params1, params2)
    }

    @Test
    fun `given params when getPokemon then getPokemon is called`() {
        /* Given */
        val params1 = 1
        val mock = mock<RestApi> {
            on { getPokemon(params1) } doReturn Observable.just(
                getFakePokemon()
            )
        }
        val sutRepository = UserDataRepository(mock)

        /* When */
        sutRepository.getPokemon(params1)

        /* Then */
        verify(mock).getPokemon(params1)
    }

    @Test
    fun `given params when getPokemonEncounterList then getPokemonEncounterList is called`() {
        /* Given */
        val params1 = 1
        val params2 = 2
        val mock = mock<RestApi> {
            on { getPokemonEncounterList(params1) } doReturn Observable.just(
                getFakePokemonEncounterList()
            )
        }
        val sutRepository = UserDataRepository(mock)

        /* When */
        sutRepository.getPokemonEncounterList(params1)

        /* Then */
        verify(mock).getPokemonEncounterList(params1)
    }

    @Test
    fun `given params when getEvolutionChain then getEvolutionChain is called`() {
        /* Given */
        val params1 = 1
        val mock = mock<RestApi> {
            on { getEvolutionChain(params1) } doReturn Observable.just(
                getFakeEvolutionChainItem()
            )
        }
        val sutRepository = UserDataRepository(mock)

        /* When */
        sutRepository.getEvolutionChain(params1)

        /* Then */
        verify(mock).getEvolutionChain(params1)
    }

}