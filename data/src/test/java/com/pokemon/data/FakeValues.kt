package com.pokemon.data

import com.pokemon.data.entity.*

object FakeValues {
    fun getFakeNamedApiResourceList() = NamedApiResourceList(

    )

    fun getFakeEvolutionChainItem() = EvolutionChain(
        id = 0,
        babyTriggerItem = null,
        chain = ChainLink(
            isBaby = true,
            species = getFakeNamedApiResource(),
            evolutionDetails = listOf(),
            evolvesTo = listOf()
        )
    )

    fun getFakeNamedApiResource() = NamedApiResource(
        name = "",
        url = "",
        category = "",
        id = 1
    )

    fun getFakePokemonEncounterList() = listOf<LocationAreaEncounter>()
    fun getFakePokemon() = Pokemon(
        id = 2,
        name = "",
        baseExperience = 2,
        height = 2,
        isDefault = false,
        order = 2,
        weight = 2,
        species = getFakeNamedApiResource(),
        abilities = listOf(),
        forms = listOf(),
        gameIndices = listOf(),
        heldItems = listOf(),
        moves = listOf(),
        stats = listOf(),
        types = listOf(),
        sprites = getfakePokeSprites()
    )

    fun getfakePokeSprites() = PokemonSprites(
        backDefault = ",",
        backFemale = "",
        frontDefault = null,
        frontFemale = null,
        backShiny = "",
        backShinyFemale = "",
        frontShiny = "",
        frontShinyFemale = ""
    )
}