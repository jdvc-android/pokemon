package com.pokemon.data.entity

data class Encounter(
    val minLevel: Int,
    val maxLevel: Int,
    val conditionValues: List<NamedApiResource>,
    val chance: Int,
    val method: NamedApiResource
)

data class VersionEncounterDetail(
    val version: NamedApiResource,
    val maxChance: Int,
    val encounterDetails: List<Encounter>
)

data class VersionGameIndex(
    val gameIndex: Int,
    val version: NamedApiResource
)